'use strict';

const routes = [
    require('./mobile/mobile.js')
    // require('./web/view.js'),
    // require('./web/create.js'),
    // require('./web/list.js'),
    // require('./web/misc.js')
];

module.exports = function router(app,db){
    return routes.forEach((route) => {
        route(app,db);
    });
};