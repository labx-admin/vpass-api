'use strict';
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');

var urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = (app,db) => {
    
}
function verifyToken(req, res, next){
    const bearerHeader = req.headers['authorization'];
    if(typeof bearerHeader !== 'undefined'){
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    } else {
        res.status(403).send({
            errorCode:"403",
            errorName:"Not Authorized",
            errorMessage:"User is not authorized"
        });
    }
}