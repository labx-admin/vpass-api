const AppUser = require("../models/app_user");
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const env = require('../config/env.config');

exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({message: "Content can not be empty!"});
    }
    else {
      AppUser.findByUser(req.body.username, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
              //HMac password
              const origPassword = req.body.password;
              let salt = crypto.randomBytes(16).toString();
              let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
              req.body.password = salt + '|' + hash;
              const appUser = new AppUser({
                username: req.body.username,
                email: req.body.email,
                contact_number: req.body.contact_number,
                password: req.body.password,
                created_at: Date.now(),
                updated_at: Date.now()
              });
              AppUser.create(appUser, null, (err, data) => {
                  if (err) 
                      res.status(500).send({
                          message: err.message || "Some error occurred while creating the user."
                      });
                  else {
                    req.body.id = data.id;
                    console.log('body', req.body);
                    const tokenDetails = {
                        id: req.body.id,
                        username: req.body.username,
                        password: origPassword
                    };
                    let token = jwt.sign(tokenDetails, env.JWT_SECRET);
                    AppUser.updateToken(req.body.id, token, (err, data) => {
                      if (err) {
                          res.status(500).send({message: "Error with token creation"})
                      }
                      else {
                          const created = Date.now();
                          res.status(200).send({
                              token: token,
                              user: {
                                id: req.body.id,
                                username: req.body.username,
                                email: req.body.email,
                                contact_number: req.body.contact_number,
                                firstname: null,
                                lastname: null,
                                //street1: null,
                                //street2: null,
                                //region: null,
                                //province: null,
                                city: null,
                                //barangay: null,
                                created_at: created,
                                updated_at: created
                            }
                          });
                      }
                    })
                  }
              });
          } else {
              res.status(500).send({message: "Error creating user"});
          }
        } else 
          res.status(400).send({
            message: "A user with the same email address has already been registered"
          });
      });
    }
};

exports.adminCreate = (req, res) => {
    const password = 'CxSB=nnt39';
    AppUser.findByUser(req.body.username, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
              //HMac password
              //req.body.username = req.body.username;
              req.body.password = password;
              let salt = crypto.randomBytes(16).toString();
              let hash = crypto.createHmac('sha512', salt).update(password).digest("base64");
              req.body.password = salt + '|' + hash;
              req.body.created_at = Date.now();
              req.body.updated_at = Date.now();
              const appUser = new AppUser(req.body);
              const appAcct = {
                  accountnumber: req.body.accountnumber,
                  accounttypeid: req.body.accounttypeid
              };
              AppUser.create(appUser, appAcct, (err, data) => {
                  if (err) 
                    res.status(500).send({
                        message: err.message || "Some error occurred while creating the user."
                    });
                  else {
                    res.status(200).send({
                        message: "User has been created"
                    })
                  }
              });
          } else {
              res.status(500).send({message: "Error creating user"});
          }
        } else 
          res.status(400).send({
            message: "A user with the same email address has already been registered"
          });
      });
};

exports.findOne = (req, res) => {
    AppUser.findBy(req.jwt.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({message: `User Not found`});
            } else {
                res.status(500).send({message: "Error retrieving user"});
            }
        } else 
            res.status(200).send(data);
    });
};

exports.getUserAccounts = (req, res) => {
    AppUser.getUserAccountsById(req.jwt.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({message: `No accounts found for user`});
            } else {
                res.status(500).send({message: "Error retrieving accounts"});
            }
        } else 
            res.status(200).send(data);
    })
}

exports.findAll = (req, res) => {
    AppUser.getAll((err, data) => {
        if (err) 
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving users."
            });
         else {
            res.status(200).send(data);
        }
    });
};


// exports.getAccountTypes = (req, res) => {
//     AppUser.getAccountTypes((err, data) => {
//         if (err) 
//             res.status(500).send({
//                 message: err.message || "Some error occurred while retrieving account types."
//             });
//          else {
//             res.status(200).send(data);
//         }
//     })
// }

exports.paginationSearch = (req, res) => {
    const searchString = req.body.searchString || null;
    AppUser.search(searchString, (err, data) => {
        if (err) 
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving users."
            });
         else {
            let page = req.body.page || 1;
            const pageSize = req.body.pageSize || 10;
            const totalPages = Math.ceil(data.length / pageSize);
            if (page > totalPages) {
                page = totalPages;
            }
            const results = data.slice((page - 1) * pageSize, (page * pageSize));
            console.log(results);
            res.status(200).send({
                totalPages: totalPages,
                page: page,
                pageSize: pageSize,
                items: results
            });
        }
    })
}

exports.getAccountDetails = (req, res) => {
    AppUser.findByUserAccountNumber(req.params.accountnumber, (err, userData) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: 'Could not find user account'
                })
            }
            else {
                res.status(500).send({
                    message: 'Error finding user account'
                })
            }
        }
        else {
            UsageHistory.getLastTenByUserAccountId(userData.useraccountid, (err, usageData) => {
                if (err) {
                    if (err.kind === "not_found") {
                        userData.usageHistory = [];
                        res.status(200).send(userData);
                    }
                    else {
                        res.status(500).send({
                            message: 'Error finding user account'
                        })
                    }
                }
                else {
                    userData.usageHistory = usageData;
                    res.status(200).send(userData);
                }
            })
        }
    })
}
// Updates user based on appUserId in parameters
// Searches for user in database to get the previous values, a new AppUser is
// created from req.body and if there are fields that are null the previous data
// is taken from the result of findBy function
exports.update = (req, res) => {
    if (!req.body) {
        res.status(400).send({message: "Content can not be empty!"});
    }
    const accountnumber = req.body.accountnumber || null;
    const userid = req.body.userid || req.jwt.id;
    if (accountnumber) {
        AppUser.findByUserAccountNumber(accountnumber, (dupErr, duplicate) => {
            let proceed = false;
            if (dupErr) {
                if (dupErr.kind != "not_found") {
                    res.status(500).send({
                        message: dupErr.message || "Error verifying account number"
                    })
                }
                else {
                    proceed = true;
                }
            }
            else {
                console.log(duplicate);
                if (duplicate.id === userid) {
                    proceed = true;
                }
                else {
                    res.status(500).send({
                        message: "Account number already assigned to another user"
                    })
                }
            }
            if (proceed) {
                let updatedFields = new AppUser(req.body);
                AppUser.findBy(userid, (err, data) => {
                    if (err) {
                        res.status(500).send({message: "Error updating User Profile"});
                    } else {
                        console.log('before pull', updatedFields);
                        for (let i in updatedFields) {
                            if (! updatedFields[i]) {
                                updatedFields[i] = data[i];
                            }
                        }
                        console.log('after pulling values', updatedFields);
                        AppUser.updateById(userid, updatedFields, accountnumber, (err, data) => {
                            if (err) {
                                if (err.kind === "not_found") {
                                    res.status(404).send({message: `User Not found.`});
                                } else {
                                    res.status(500).send({message: "Error updating user"});
                                }
                            } else 
                                res.status(200).send(data);
                        });
                    }
                })
            }
        })
    }
    else {
        let updatedFields = new AppUser(req.body);
        AppUser.findBy(userid, (err, data) => {
            if (err) {
                res.status(500).send({message: "Error updating User Profile"});
            } else {
                console.log('before pull', updatedFields);
                for (let i in updatedFields) {
                    if (! updatedFields[i]) {
                        updatedFields[i] = data[i];
                    }
                }
                console.log('after pulling values', updatedFields);
                AppUser.updateById(userid, updatedFields, null, (err, data) => {
                    if (err) {
                        if (err.kind === "not_found") {
                            res.status(404).send({message: `User Not found.`});
                        } else {
                            res.status(500).send({message: "Error updating user"});
                        }
                    } else 
                        res.status(200).send(data);
                });
            }
        })
    }
};

exports.delete = (req, res) => {
    AppUser.remove(req.jwt.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({message: `User Not found.`});
            } else {
                res.status(500).send({message: "Could not delete user"});
            }
        } else 
            res.send({message: `User was deleted successfully!`});
        

    });
};

exports.deleteOtherUser = (req, res) => {
  if (!req.params && !req.params.id) {
    res.status(400).send({
      message: "Invalid parameters"
    });
  }
  else {
    console.log("controller params: ", req.params);
    AppUser.remove(req.params.id, (err, data) =>{
      if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `User Not found.`
            });
          } else {
            res.status(500).send({
              message: "Could not delete user"
            });
          }
        } else res.send({ message: `User was deleted successfully!` });
    });
  }
};

exports.adminEditUser = (req, res) => {
    AppUser.findByUserAccountNumber(req.body.accountnumber, (err, verifyData) => {
        if (err) {
            if (err.kind === "not_found") {

            }
            else {
                res.status(500).send({
                    message: err.message || "Error verifying account number"
                })
            }
        }
        else {
            res.status(500).send({
                message: "Account Number already exists"
            })
        }
    })
}