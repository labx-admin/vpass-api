const User = require("../models/user");
const jwt = require('jsonwebtoken');
const env = require("../config/env.config");

//Checks if the Authorization token exists and is valid
exports.verifyToken = (req, res, next) => {
    console.log(req.headers);
    if (req.headers['authorization']) {
        try {
            let auth = req.headers['authorization'].split(' ');
            if (auth[0] !== 'Bearer') {
                res.status(403).send({
                    message: "Unauthorized Access"
                });
            } else {
                req.jwt = jwt.verify(auth[1], env.JWT_SECRET);
                User.findBy(req.jwt.id, (err, data) => {
                    if (err) {
                        res.status(500).send({message: "Error verifying token"});
                    }
                    else {
                        if (data.accessToken != auth[1]) {
                            res.status(403).send({
                                message: "Unauthorized Access"
                            });
                        }
                        else {
                            return next();
                        }
                    }
                })
            }
        }
        catch (err) {
            res.status(403).send({
                message: "Unauthorized Access"
            });
        }
    } else {
        res.status(401).send({
            message: "Unauthorized Access"
        });
    }
}

exports.verifyFacilitator = (req, res, next) => {
    User.findRole(req.jwt.id, (err, data) => {
        if (err) {
            res.status(500).send({
                message: "Error authenticating user"
            });
        }
        else {
            console.log(data);
            if (data.name != "Facilitator") {
                res.status(401).send({
                    message: "Unauthorized Access"
                });
            }
            else {
                return next();
            }
        }
    })
}

exports.verifyCheckpoint = (req, res, next) => {
    User.findRole(req.jwt.id, (err, data) => {
        if (err) {
            res.status(500).send({
                message: "Error authenticating user"
            });
        }
        else {
            console.log(data);
            if (data.name === "AppUser") {
                res.status(401).send({
                    message: "Unauthorized Access"
                });
            }
            else {
                return next();
            }
        }
    })
}