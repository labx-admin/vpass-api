const User = require("../models/user");
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const env = require("../config/env.config");

//Verifies if the password matches what is in the database
exports.verifyPassword = (req, res, next) => {
    if (!req.body) {
      res.status(400).send({message: "Content cannot be empty"});
    }
    User.findByUser(req.body.username, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(400).send({message: `Invalid username or password`});
        } else {
            res.status(500).send({message: "Error retrieving user"});
        }
      } else {
        let passwordSplit = data.password.split('|');
        let salt = passwordSplit[0];
        let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
        console.log(hash);
        console.log(passwordSplit[1]);
        if (hash === passwordSplit[1]) {
            req.body.id = data.id;
            return next();
        } else {
          res.status(401).send({message: "Invalid username or password"});
        }
      }
    });
}

//Verifies if body contains username and password fields
exports.validateAuthFields = (req, res, next) => {
    if (req.body) {
        if (!req.body.username) {
            res.status(400).send({
                message: "Missing username"
            });
        } else
        if (!req.body.password) {
            res.status(400).send({
                message: "Missing password"
            });
        } else {
            return next();
        }
    } else {
        res.status(400).send({
            message: "Missing email and password"
        });
    }
}

//Creates JWT token for authorization
exports.login = (req, res) => {
    console.log('auth', req.body);
    try {
        const tokenDetails = {
            id: req.body.id,
            username: req.body.username,
            password: req.body.password
        };
        let token = jwt.sign(tokenDetails, env.JWT_SECRET);
        User.findBy(req.body.id, (err, data) => {
            if (err) {
                console.log(err);
                res.status(500).send({message: "Error checking user"})
            }
            else {
                console.log('update token');
                if (!data.isAccepted) {
                    console.log('isAccepted is null');
                    User.toggleAccepted(req.body.id, (err, updateres) => {
                        if (err) {
                            console.log("Error updating isAccepted");
                        }
                    })
                }
                if (req.body.deviceId && data.deviceId !== req.body.deviceId) {
                    User.updateDeviceId(req.body.id, req.body.deviceId, (err, updateId) => {
                        if (err) {
                            console.log("Error updating deviceId");
                        }
                    })
                }
                User.updateToken(req.body.id, token, (err, d) => {
                    if (err) {
                        res.status(500).send({message: "Error with token creation"})
                    }
                    else {
                        console.log('find role');
                        User.findRole(req.body.id, (err, role) => {
                            if (err) {
                                res.status(500).send({message: "Error retrieving role"})
                            }
                            else {
                                console.log('get guid');
                                User.getGuid(req.body.id, (err, userGuid) => {
                                    if (err) {
                                        res.status(500).send({message: "Error retrieving role"})
                                    }
                                    else {
                                        res.status(200).send({
                                            token: token,
                                            guid: userGuid.guid,
                                            user: {
                                                    id: req.body.id,
                                                    username: data.username,
                                                    email: data.email,
                                                    role: role.name,
                                                    firstname: data.firstname,
                                                    lastname: data.lastname,
                                                    contact_number: data.contact_number,
                                                    city_address: data.city_address
                                            }
                                        });
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
    catch (err) {
        console.log(err);
        res.status(500).send({
            message: err
        });
    }
}

exports.loginAdmin = (req, res) => {
    console.log('auth', req.body);
    try {
        const tokenDetails = {
            id: req.body.id,
            username: req.body.username,
            password: req.body.password
        };
        let token = jwt.sign(tokenDetails, env.JWT_SECRET);
        User.findBy(req.body.id, (err, data) => {
            if (err) {
                console.log(err);
                res.status(500).send({message: "Error checking user"})
            }
            else {
                console.log('update token');
                if (!data.isAccepted) {
                    console.log('isAccepted is null');
                    User.toggleAccepted(req.body.id, (err, updateres) => {
                        if (err) {
                            console.log("Error updating isAccepted");
                        }
                    })
                }
                if (req.body.deviceId && data.deviceId !== req.body.deviceId) {
                    User.updateDeviceId(req.body.id, req.body.deviceId, (err, updateId) => {
                        if (err) {
                            console.log("Error updating deviceId");
                        }
                    })
                }
                User.updateToken(req.body.id, token, (err, d) => {
                    if (err) {
                        res.status(500).send({message: "Error with token creation"})
                    }
                    else {
                        console.log('find role');
                        User.findRole(req.body.id, (err, role) => {
                            if (err) {
                                res.status(500).send({message: "Error retrieving role"})
                            }
                            else if (role.name !== "Facilitator") {
                                res.status(403).send({message: "Unauthorized Access"})
                            }
                            else {
                                console.log('get guid');
                                User.getGuid(req.body.id, (err, userGuid) => {
                                    if (err) {
                                        res.status(500).send({message: "Error retrieving role"})
                                    }
                                    else {
                                        res.status(200).send({
                                            token: token,
                                            guid: userGuid.guid,
                                            user: {
                                                    id: req.body.id,
                                                    username: data.username,
                                                    email: data.email,
                                                    role: role.name,
                                                    firstname: data.firstname,
                                                    lastname: data.lastname,
                                                    contact_number: data.contact_number,
                                                    city_address: data.city_address
                                            }
                                        });
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
    catch (err) {
        console.log(err);
        res.status(500).send({
            message: err
        });
    }
}

//Deletes access token in db
exports.logout = (req, res) => {
    User.deleteToken(req.jwt.id, (err, data) => {
        if (err) {
            res.status(500).send({
                message: "Error logging out"
            });
        }
        else {
            res.status(200).send({
                message: "Successfully logged out"
            });
        }
    })
}