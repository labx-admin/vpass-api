const Test = require('../models/facilitator');
const xlsxPop = require('xlsx-populate');

exports.getUserData = (req, res) => {
    Test.getUserData(req.params.guid, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: "Error reading GUID"
                })
            }
            else {
                res.status(500).send({
                    message: err.message || "Error retrieving user data"
                })
            }
        }
        else {
            res.status(200).send(data);
        }
    })
}

exports.searchUser = (req, res) => {
    Test.searchUser(req.params.query, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(200).send([]);
            }
            else {
                res.status(500).send({
                    message: err.message || "Error searching user data"
                })
            }
        }
        else {
            res.status(200).send(data);
        }
    })
}

exports.addResult = (req, res) => {
    console.log(req.body);
    const newTest = {
        virusttypeid: req.body.virusttypeid,
        testtypeid: req.body.testtypeid,
        resulttype: req.body.resulttypeid,
        date_recorded: req.body.dateRecorded,
        remarks: req.body.remarks,
        isDownloadable: req.body.isDownloadable
    }
    Test.create(req.jwt.id, req.body.guid, newTest, (err, data) => {
        if (err) {
            res.status(500).send({
                message: err.message || "Error adding test history to user"
            })
        }
        else {
            res.status(200).send({
                message: "Successfully added test history"
            })
        }
    })
}

exports.toggleDownloadable = (req, res) => {
    Test.toggleDownloadable(req.body.guid, (err, data) => {
        if (err) {
            res.status(500).send({
                message: err.message || "Error toggling downloadable for test"
            });
        }
        else {
            res.status(200).send({
                message: "Successfully toggled downloadable flag"
            })
        }
    })
}

exports.getData = (req, res) => {
    Test.getData((err, data) => {
        if (err) {
            res.status(500).send({
                message: err.message || "Error retrieving test data"
            })
        }
        else {
            console.log(data);
            let virusData = [];
            let viruses = [];
            let tests = [];
            data.forEach(item => {
                if (!viruses.includes(item.virus)) {
                    viruses.push(item.virus);
                    tests = [item.test];
                    const newVirus = {
                        virusttypeid: item.virusttypeid,
                        virus: item.virus,
                        tests: [
                            {
                                testtypeid: item.testtypeid,
                                testName: item.test,
                                results: [
                                    {
                                        resulttypeid: item.resulttypeid,
                                        result: item.result,
                                        is_safe: item.is_safe
                                    }
                                ]
                            }
                        ]
                    }
                    virusData.push(newVirus);
                }
                else if (!tests.includes(item.test)) {
                    tests.push(item.test);
                    const newTest = {
                        testtypeid: item.testtypeid,
                        testName: item.test,
                        results: [
                            {
                                resulttypeid: item.resulttypeid,
                                result: item.result,
                                is_safe: item.is_safe
                            }
                        ]
                    }
                    virusData.forEach(virusItem => {
                        if (virusItem.virus === item.virus) {
                            virusItem.tests.push(newTest);
                        }
                    })
                }
                else {
                    virusData.forEach(virusItem => {
                        if (virusItem.virus === item.virus) {
                            virusItem.tests.forEach(testItem => {
                                if (testItem.testName === item.test) {
                                    testItem.results.push({
                                        resulttypeid: item.resulttypeid,
                                        result: item.result,
                                        is_safe: item.is_safe
                                    })
                                }
                            })
                        }
                    })
                }
            });
            res.status(200).send(virusData);
        }
    })
}

exports.getResults = (req, res) => {
    Test.getTestResult(req.jwt.id, req.params.guid, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: "Error reading GUID"
                })
            }
            else {
                res.status(500).send({
                    message: err.message || "Error retrieving test results"
                })
            }
        }
        else {
            // if (data.is_safe === 0) {
            //     data.is_safe = 1;
            // }
            // else if (data.is_safe === 1) {
            //     data.is_safe = 0;
            // }
            res.status(200).send(data);
        }
    })
}

exports.getAllUserResults = (req, res) => {
    Test.getAllUserResults(req.params.guid, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: "Error reading GUID"
                })
            }
            else {
                res.status(500).send({
                    message: err.message || "Error retrieving test results"
                })
            }
        }
        else {
            res.status(200).send(data);
        }
    })
}

exports.getHTMLResults = (req, res) => {
    Test.getTestResult(null, req.params.guid, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: "Error reading GUID"
                })
            }
            else {
                res.status(500).send({
                    message: err.message || "Error retrieving test results"
                })
            }
        }
        else {
            const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            let conv_date = null;
            if (data.date_recorded) {
                const date = new Date(data.date_recorded);
                let time = ' AM';
                let hour = date.getHours();
                if (hour > 12) {
                    hour = hour - 12;
                    time = ' PM';
                }
                if (hour === 12) {
                    time = ' PM';
                }
                conv_date = (months[date.getMonth()]) + ' ' +
                (date.getDate()) + ', ' +
                (date.getFullYear()) + ' ' +
                hour + ':' +
                (date.getMinutes()) + time;
            }
            const colors = ["red", "blue", "black"];
            res.render("index", {
                virus: data.virus,
                test: data.test,
                firstname: data.firstname,
                lastname: data.lastname,
                city_address: data.city_address,
                contact_number: data.contact_number,
                date_recorded: conv_date,
                safe: data.is_safe,
                color: colors[data.is_safe]
            });
        }
    })
}

exports.getReport = (req, res) => {
    Test.getResultsByMonth(req.params.month, (err, data) => {
        xlsxPop.fromBlankAsync().then(workbook => {
            let row = 2;
            workbook.sheet(0).cell("A1").value("Date Recorded");
            workbook.sheet(0).cell("B1").value("Virus");
            workbook.sheet(0).cell("C1").value("Test");
            workbook.sheet(0).cell("D1").value("Result");
            workbook.sheet(0).cell("E1").value("Is Safe?");

            data.forEach(item => {
                workbook.sheet(0).cell("A" + row).value(new Date(item.date_recorded)).style("numberFormat", "mmmm dd, yyyy HH:mm");
                workbook.sheet(0).cell("B" + row).value(item.virus);
                workbook.sheet(0).cell("C" + row).value(item.test);
                workbook.sheet(0).cell("D" + row).value(item.result);
                workbook.sheet(0).cell("E" + row).value(item.is_safe);
                row++;
            });

            return workbook.outputAsync();
        }).then((result) => {
            res.attachment(`history_report_${req.params.month}.xlsx`);
            res.status(200).send(result);
        })
    })
}

exports.getWebResults = (req, res) => {
    let id = null;
    if (req.jwt && req.jwt.id) {
        id = req.jwt.id;
    }
    Test.getTestResult(id, req.params.guid, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: "Error reading GUID"
                })
            }
            else {
                res.status(500).send({
                    message: err.message || "Error retrieving test results"
                })
            }
        }
        else {
            // if (data.is_safe === 0) {
            //     data.is_safe = 1;
            // }
            // else if (data.is_safe === 1) {
            //     data.is_safe = 0;
            // }
            res.status(200).send(data);
        }
    })
}

exports.updateResult = (req, res) => {
    const body = req.body;
    Test.updateResult(body.guid, body.virusttypeid, body.testtypeid, body.resulttypeid, body.date_recorded, body.isDownloadable, (err, data) => {
        if (err) {
            res.status(500).send({
                message: "Error updating test result"
            });
        }
        else {
            res.status(200).send({
                message: "Updated test result"
            });
        }
    })
}

exports.getLatestUsers = (req, res) => {
    Test.getLatestUsers((err, data) => {
        if (err) {
            res.status(500).send({
                message: "Error retrieving users"
            });
        }
        else {
            res.status(200).send(data);
        }
    })
}

exports.getUsersPagination = (req, res) => {
    const body = req.body;
    Test.getUsersPagination(body.page, body.query, (err, data) => {
        if (err) {
            res.status(500).send({
                message: "Error retrieving users"
            });
        }
        else {
            res.status(200).send(data);
        }
    })
}

exports.getTestByGuid = (req, res) => {
    Test.getTestByGuid(req.params.guid, (err, data) => {
        if (err) {
            res.status(500).send({
                message: "Error retrieving test result"
            });
        }
        else {
            res.status(200).send(data);
        }
    })
}

exports.deleteTest = (req, res) => {
    Test.deleteTest(req.params.guid, (err, data) => {
        if (err) {
            res.status(500).send({
                message: "Error deleting tet result"
            });
        }
        else {
            res.status(200).send({
                message: "Test Result deleted"
            })
        }
    })
}