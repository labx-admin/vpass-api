const AppUser = require('../models/user');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const env = require("../config/env.config");
const pdf = require('../models/pdf');
const mailer = require('../models/mailer');
const fs = require('fs');

exports.register = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content cannot be empty"
        })
    }
    else {
        console.log(req.body);
        AppUser.checkUnique(req.body.email, (err, uniqueUser) => {
            if (err) {
                if (err.kind === "not_found") {
                    let salt = crypto.randomBytes(16).toString();
                    while (salt.includes('|')) {
                        salt = crypto.randomBytes(16).toString();
                    }
                    const origPassword = req.body.password;
                    let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
                    req.body.password = salt + '|' + hash;
                    const newUser = new AppUser({
                        username: req.body.email,
                        email: req.body.email,
                        password: req.body.password,
                        firstname: req.body.firstname,
                        lastname: req.body.lastname,
                        contact_number: req.body.contact_number,
                        city_address: req.body.city_address
                    });
                    AppUser.create(1, newUser, null, (err, data) => {
                        if (err) {
                            res.status(500).send({
                                message: err.message || "Error occured creating user"
                            })
                        }
                        else {
                            const tokenDetails = {
                                id: data.id,
                                username: newUser.username,
                                password: origPassword
                            }
                            const token = jwt.sign(tokenDetails, env.JWT_SECRET);
                            AppUser.updateToken(data.id, token, (err, tokenRes) => {
                                if (err) {
                                    res.status(500).send({
                                        message: err.message || "Error occured creating user"
                                    })
                                }
                                else {
                                    res.status(200).send({
                                        token: token,
                                        guid:  data.guid,
                                        user: {
                                            id: data.id,
                                            username: newUser.username,
                                            email: newUser.email,
                                            role: "AppUser",
                                            firstname: newUser.firstname,
                                            lastname: newUser.lastname,
                                            contact_number: newUser.contact_number,
                                            city_address: newUser.city_address,
                                            facebookid: null,
                                            googleid: null
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
                else {
                    res.status(500).send({
                        message: err.message || "Error validating user"
                    });
                }
            }
            else {
                res.status(400).send({
                    message: "A user with the same email or contact number has already been registered"
                })
            }
        })
    }
}

exports.delete = (req, res) => {
    AppUser.delete(req.params.id, (err, data) => {
        if (err) {
            res.status(500).send({
                message: "Error deleting user"
            });
        }
        else {
            res.status(200).send({
                message: "User was successfully deleted"
            })
        }
    })
}

exports.update = (req, res) => {
    const body = req.body;
    console.log(body);
    AppUser.edit(body.guid, body.firstname, body.lastname, body.email, body.contact, (err, data) => {
        if (err) {
            res.status(500).send({
                message: "Error updating user details"
            });
        }
        else {
            res.status(200).send({
                message: "User details were successfully updated"
            })
        }
    })
}

exports.resetEmail = (req, res) => {
    const email = req.params.email;
    mailer.sendResetEmail(email, (err, data) => {
        if (err) {
            res.status(500).send({
                message: "Error sending reset password email"
            });
        }
        else {
            res.status(200).send({
                message: "Reset password email has been sent"
            })
        }
    })
}

exports.resetPassword = (req, res) => {
    // req.body.password = "QWerty123!";
    // let salt = crypto.randomBytes(16).toString();
    // while (salt.includes('|')) {
    //     salt = crypto.randomBytes(16).toString();
    // }
    // let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
    // req.body.password = salt + '|' + hash;
    // AppUser.updatePassword(req.body.id, req.body.password, (err, data) => {
    //     if (err) {
    //         res.status(500).send({
    //             message: "Error resetting password"
    //         })
    //     }
    //     else {
    //         res.status(200).send({
    //             message: "Password reset"
    //         })
    //     }
    // })
    const body = req.body;
    let salt = crypto.randomBytes(16).toString();
    while (salt.includes('|')) {
        salt = crypto.randomBytes(16).toString();
    }
    let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
    req.body.password = salt + '|' + hash;
    AppUser.updatePassword(body.email, body.password, body.otp, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(403).send({
                    message: "Invalid details"
                })
            }
            else {
                res.status(500).send({
                    message: "Error resetting password"
                })
            }
        }
        else {
            res.status(200).send({
                message: "Password has been reset"
            })
        }
    })
}

exports.getTestResults = (req, res) => {
    AppUser.getTests(req.jwt.id, (err, data) => {
        if (err) {
            res.status(500).send({
                message: "Error retrieving test results"
            })
        }
        else {
            // data.forEach(item => {
            //     item.downloadAvailable = false;
            // });
            res.status(200).send(data);
        }
    })
}

exports.gfbSignin = (req, res) => {
    AppUser.findByUser(req.body.email, (err, uniqueUser) => {
        if (err) {
            if (err.kind === "not_found") {
                const newUser = new AppUser({
                    username: req.body.email,
                    email: req.body.email,
                    password: null,
                    firstname: req.body.displayName,
                    lastname: null,
                    contact_number: req.body.phonenumber || null,
                    city_address: req.body.address || null,
                    facebookid: req.body.facebookid || null,
                    googleid: req.body.googleid || null,
                    appleid: req.body.appleid || null
                });
                AppUser.create(1, newUser, null, (err, data) => {
                    if (err) {
                        res.status(500).send({
                            message: err.message || "Error occured creating user"
                        })
                    }
                    else {
                        const tokenDetails = {
                            id: data.id,
                            username: newUser.username,
                            password: null
                        }
                        const token = jwt.sign(tokenDetails, env.JWT_SECRET);
                        AppUser.updateToken(data.id, token, (err, tokenRes) => {
                            if (err) {
                                res.status(500).send({
                                    message: err.message || "Error occured creating user"
                                })
                            }
                            else {
                                res.status(200).send({
                                    token: token,
                                    guid:  data.guid,
                                    user: {
                                        id: data.id,
                                        username: newUser.username,
                                        email: newUser.email,
                                        role: "AppUser",
                                        firstname: newUser.firstname,
                                        lastname: newUser.lastname,
                                        contact_number: newUser.contact_number,
                                        city_address: newUser.city_address,
                                        facebookid: newUser.facebookid,
                                        googleid: newUser.googleid,
                                        appleid: newUser.appleid
                                    }
                                })
                            }
                        })
                    }
                })
            }
            else {
                res.status(500).send({
                    message: err.message || "Error validating user"
                });
            }
        }
        else {
            if ((req.body.facebookid && uniqueUser.facebookid && uniqueUser.facebookid === req.body.facebookid) || (req.body.googleid && uniqueUser.googleid && req.body.googleid === uniqueUser.googleid) || (req.body.appleid && uniqueUser.appleid && req.body.appleid === uniqueUser.appleid)) {
                const tokenDetails = {
                    id: uniqueUser.id,
                    username: uniqueUser.username,
                    password: null
                }
                let token = jwt.sign(tokenDetails, env.JWT_SECRET);
                AppUser.updateToken(uniqueUser.id, token, (err, d) => {
                    if (err) {
                        res.status(500).send({message: "Error with token creation"})
                    }
                    else {
                        AppUser.findRole(uniqueUser.id, (err, role) => {
                            if (err) {
                                res.status(500).send({message: "Error retrieving role"})
                            }
                            else {
                                console.log('get guid');
                                AppUser.getGuid(uniqueUser.id, (err, userGuid) => {
                                    if (err) {
                                        res.status(500).send({message: "Error retrieving role"})
                                    }
                                    else {
                                        res.status(200).send({
                                            token: token,
                                            guid: userGuid.guid,
                                            user: {
                                                    id: uniqueUser.id,
                                                    username: uniqueUser.username,
                                                    email: uniqueUser.email,
                                                    role: role.name,
                                                    firstname: uniqueUser.firstname,
                                                    lastname: uniqueUser.lastname,
                                                    contact_number: uniqueUser.contact_number,
                                                    city_address: uniqueUser.city_address,
                                                    facebookid: uniqueUser.facebookid,
                                                    googleid: uniqueUser.googleid,
                                                    appleid: uniqueUser.appleid
                                            }
                                        });
                                    }
                                })
                            }
                        })
                    }
                })
            }
            else if ((req.body.facebookid && uniqueUser.facebookid === null) || (req.body.googleid && uniqueUser.googleid === null) || (req.body.appleid && uniqueUser.appleid === null)) {
                AppUser.updateGFbId(uniqueUser.id, req.body.googleid, req.body.facebookid, req.body.appleid, (err, updateRes) => {
                    if (err) {
                        res.status(500).send({message: "Error updating id"})
                    }
                    else {
                        if (req.body.facebookid) {
                            uniqueUser.facebookid = req.body.facebookid
                        }
                        if (req.body.googleid) {
                            uniqueUser.googleid = req.body.googleid
                        }
                        if (req.body.appleid) {
                            uniqueUser.appleid = req.body.appleid
                        }
                        const tokenDetails = {
                            id: uniqueUser.id,
                            username: uniqueUser.username,
                            password: null
                        }
                        let token = jwt.sign(tokenDetails, env.JWT_SECRET);
                        AppUser.updateToken(uniqueUser.id, token, (err, d) => {
                            if (err) {
                                res.status(500).send({message: "Error with token creation"})
                            }
                            else {
                                AppUser.findRole(uniqueUser.id, (err, role) => {
                                    if (err) {
                                        res.status(500).send({message: "Error retrieving role"})
                                    }
                                    else {
                                        console.log('get guid');
                                        AppUser.getGuid(uniqueUser.id, (err, userGuid) => {
                                            if (err) {
                                                res.status(500).send({message: "Error retrieving role"})
                                            }
                                            else {
                                                res.status(200).send({
                                                    token: token,
                                                    guid: userGuid.guid,
                                                    user: {
                                                            id: uniqueUser.id,
                                                            username: uniqueUser.username,
                                                            email: uniqueUser.email,
                                                            role: role.name,
                                                            firstname: uniqueUser.firstname,
                                                            lastname: uniqueUser.lastname,
                                                            contact_number: uniqueUser.contact_number,
                                                            city_address: uniqueUser.city_address,
                                                            facebookid: uniqueUser.facebookid,
                                                            googleid: uniqueUser.googleid,
                                                            appleid: uniqueUser.appleid
                                                    }
                                                });
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
            else {
                res.status(403).send({
                    message: "Invalid sign in credentials"
                })
            }
        }
    })
}

exports.registerFacilitator = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content cannot be empty"
        })
    }
    else {
        AppUser.checkUnique(req.body.email, (err, uniqueUser) => {
            if (err) {
                if (err.kind === "not_found") {
                    let salt = crypto.randomBytes(16).toString();
                    while (salt.includes('|')) {
                        salt = crypto.randomBytes(16).toString();
                    }
                    const origPassword = req.body.password;
                    let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
                    req.body.password = salt + '|' + hash;
                    const newUser = new AppUser({
                        username: req.body.email,
                        email: req.body.email,
                        password: req.body.password,
                        firstname: req.body.firstname,
                        lastname: req.body.lastname || null,
                        contact_number: req.body.contact_number,
                        city_address: req.body.city_address || null
                    });
                    AppUser.create(2, newUser, req.body.prc_number, (err, data) => {
                        if (err) {
                            res.status(500).send({
                                message: err.message || "Error occured creating user"
                            })
                        }
                        else {
                            res.status(200).send({
                                message: "New facilitator has been registered"
                            })
                        }
                    })
                }
                else {
                    res.status(500).send({
                        message: err.message || "Error validating user"
                    });
                }
            }
            else {
                res.status(400).send({
                    message: "A user with the same email or contact number has already been registered"
                })
            }
        })
    }
}

exports.registerCheckpoint = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content cannot be empty"
        })
    }
    else {
        AppUser.checkUnique(req.body.email, (err, uniqueUser) => {
            if (err) {
                if (err.kind === "not_found") {
                    let salt = crypto.randomBytes(16).toString();
                    while (salt.includes('|')) {
                        salt = crypto.randomBytes(16).toString();
                    }
                    const origPassword = req.body.password;
                    let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
                    req.body.password = salt + '|' + hash;
                    const newUser = new AppUser({
                        username: req.body.email,
                        email: req.body.email,
                        password: req.body.password,
                        firstname: req.body.firstname,
                        lastname: req.body.lastname || null,
                        contact_number: req.body.contact_number,
                        city_address: req.body.city_address
                    });
                    AppUser.create(3, newUser, null, (err, data) => {
                        if (err) {
                            res.status(500).send({
                                message: err.message || "Error occured creating user"
                            })
                        }
                        else {
                            const tokenDetails = {
                                id: data.id,
                                username: newUser.username,
                                password: origPassword
                            }
                            const token = jwt.sign(tokenDetails, env.JWT_SECRET);
                            AppUser.updateToken(data.id, token, (err, tokenRes) => {
                                if (err) {
                                    res.status(500).send({
                                        message: err.message || "Error occured creating user"
                                    })
                                }
                                else {
                                    res.status(200).send({
                                        token: token,
                                        guid:  data.guid,
                                        user: {
                                            id: data.id,
                                            username: newUser.username,
                                            email: newUser.email,
                                            role: "Checkpoint",
                                            firstname: newUser.firstname,
                                            lastname: newUser.lastname,
                                            contact_number: newUser.contact_number,
                                            city_address: newUser.city_address
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
                else {
                    res.status(500).send({
                        message: err.message || "Error validating user"
                    });
                }
            }
            else {
                res.status(400).send({
                    message: "A user with the same email or contact number has already been registered"
                })
            }
        })
    }
}

exports.getGuid = (req, res) => {
    AppUser.getGuid(req.jwt.id, (err, data) => {
        if (err) {
            res.status(500).send({
                message: err.message | "Error retrieving guid"
            })
        }
        else {
            res.status(200).send(data);
        }
    })
}

exports.contactTracing = (req, res) => {
    if (!req.body.targetguid) {
        res.status(400).send({
            message: "No target defined"
        });
    }
    else {
        AppUser.contactTrace(req.jwt.id, req.body.targetguid, (err, data) => {
            if (err) {
                res.status(500).send({
                    message: err.message | "Error saving contact trace details"
                });
            }
            else {
                res.status(200).send({
                    message: "Contact tracing was successful"
                })
            }
        })
    }
}

exports.downloadPDF = (req, res) => {
    pdf.testPdf(req.jwt.id, req.params.guid, false, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: "Error reading GUID"
                })
            }
            else {
                res.status(500).send({
                    message: err.message || "Error retrieving test results"
                })
            }
        }
        else {
            res.status(200).sendFile(data, err => {
                if (err) {
                    console.log(err);
                    res.status(500).send({
                        message: "Error generating test certificate"
                    });
                }
                fs.unlink(data, function(err) {
                    if (err) {
                        console.log(err);
                    }
                })
            });
        }
    })
}

exports.downloadOtherPDF = (req, res) => {
    pdf.testPdf(req.jwt.id, req.params.guid, true, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: "Error reading GUID"
                })
            }
            else {
                res.status(500).send({
                    message: err.message || "Error retrieving test results"
                })
            }
        }
        else {
            res.status(200).sendFile(data, err => {
                if (err) {
                    console.log(err);
                    res.status(500).send({
                        message: "Error generating test certificate"
                    });
                }
                fs.unlink(data, function(err) {
                    if (err) {
                        console.log(err);
                    }
                })
            });
        }
    })
}

exports.mobileDownload = (req, res) => {
    pdf.testPdf(req.jwt.id, req.params.guid, false, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: "Error reading GUID"
                })
            }
            else {
                res.status(500).send({
                    message: err.message || "Error retrieving test results"
                })
            }
        }
        else {
            const readStream = fs.createReadStream(data);
            res.writeHead(200, {'Content-Type': 'application/pdf'});
            readStream.pipe(res);
            fs.unlink(data, function(err) {
                if (err) {
                    console.log(err);
                }
            })
        }
    })
}