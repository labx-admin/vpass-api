const express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    swaggerui = require('swagger-ui-express');
    APIDocument = require('./documentation/swagger');
    env = require('./config/env.config');
    cors = require('cors');
    ejs = require('ejs');

// const { check } = require('express-validator');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.json());
app.use('/api-docs', swaggerui.serve, swaggerui.setup(APIDocument));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use(cors());
app.use(express.static(__dirname));
app.set("view engine", "ejs");
app.set("views", __dirname + "/views");

const port = 3000;

app.get("/", (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.get("/privacypolicy", (req, res) => {
    res.sendFile(__dirname + "/privacy-policy.html");
});

app.get("/termsandconditions", (req, res) => {
    res.sendFile(__dirname + "/terms.html");
});

app.get("/register", (req, res) => {
    res.sendFile(__dirname + "/register.html");
});

app.get("/register/success", (req, res) => {
    res.sendFile(__dirname + "/register-success.html");
});

require("./routes/user.route")(app);
require("./routes/facilitator.route")(app);
require("./routes/checkpoint.route")(app);

app.listen(port, () => {
    console.log('VPass @ localhost:3000/');
});


