const sql = require("./db");
const crypto = require('crypto');
const moment = require("moment");

const User = function (user) {
    this.username = user.username;
    this.email = user.email;
    this.password = user.password;
    this.accessToken = user.accessToken || null;
    this.firstname = user.firstname;
    this.lastname = user.lastname;
    this.contact_number = user.contact_number || null;
    this.city_address = user.city_address || null;
    this.facebookid = user.facebookid || null;
    this.googleid = user.googleid || null;
    this.appleid = user.appleid || null;
    this.isAccepted = true;
    this.deviceId = user.deviceId || null;
}

User.create = (type, user, prc_number, result) => {
    sql.query(`INSERT INTO app_user SET ?`, user, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else {
            console.log('insertData', res);
            const newUserId = res.insertId;
            sql.query(`INSERT INTO app_user_role SET appuserid = ?, userroleid = ?`, [ parseInt(newUserId), parseInt(type) ],(err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                else {
                    sql.query(`SELECT guid FROM app_user_guid`, (err, guidList) => {
                        if (err) {
                            console.log("error: ", err);
                            result(err, null);
                            return;
                        }
                        else {
                            let newGuid = crypto.randomBytes(10).toString('hex');
                            while(guidList.includes(newGuid)) {
                                newGuid = crypto.randomBytes(10).toString('hex');
                            }
                            sql.query(`INSERT INTO app_user_guid SET appuserid = ?, guid = ?`, [ parseInt(newUserId) , newGuid.toString() ], (err, guidRes) => {
                                console.log('check user: ' + JSON.stringify(user));
                                if (err) {
                                    console.log("error: ", err);
                                    result(err, null);
                                    return;
                                }
                                else if (prc_number) {
                                    sql.query(`INSERT INTO facilitator_prc SET appuserid = ?, prc_number = ?`, [parseInt(newUserId), prc_number.toString()], (err, prcRes) => {
                                        if (err) {
                                            console.log("error: ", err);
                                            result(err, null);
                                            return;
                                        }
                                        else {
                                            result(null, {
                                                id: newUserId,
                                                guid: newGuid,
                                                ...user
                                            })
                                        }
                                    })
                                }
                                else {
                                    result(null, {
                                        id: newUserId,
                                        guid: newGuid,
                                        ...user
                                    });
                                }  
                            })
                        }
                    })
                }
            })
        }
    });
}

User.edit = (userGuid, firstname, lastname, email, contact, result) => {
    sql.query(`SELECT appuserid from app_user_guid WHERE guid = ?`, userGuid, (err, userGuidExtract) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else {
            const userid = userGuidExtract[0].appuserid;
            const details = {
                firstname: firstname,
                lastname: lastname,
                email: email,
                contact_number: contact
            }
            sql.query(`UPDATE app_user SET ? WHERE id = ?`, [details, userid], (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                else {
                    result(null, res);
                }
            })
        }
    });
}

User.delete = (userGuid, result) => {
    sql.query(`SELECT appuserid from app_user_guid WHERE guid = ?`, userGuid, (err, userGuidExtract) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else {
            console.log(userGuidExtract);
            const userid = userGuidExtract[0].appuserid;
            console.log(userid);
            sql.query(`DELETE FROM history WHERE appuserid = ?`, userid, (err, hisdel) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                else {
                    sql.query(`DELETE FROM app_user_guid WHERE appuserid = ?`, userid, (err, guiddel) => {
                        if (err) {
                            console.log("error: ", err);
                            result(err, null);
                            return;
                        }
                        else {
                            sql.query(`DELETE FROM app_user_role WHERE appuserid = ?`, userid, (err, roledel) => {
                                if (err) {
                                    console.log("error: ", err);
                                    result(err, null);
                                    return;
                                }
                                else {
                                    sql.query(`DELETE FROM contact_tracing WHERE sourceid = ? OR targetid = ?`, [userid, userid], (err, contactdel) => {
                                        if (err) {
                                            console.log("error: ", err);
                                            result(err, null);
                                            return;
                                        }
                                        else {
                                            sql.query(`DELETE FROM app_user WHERE id = ?`, userid, (err, res) => {
                                                if (err) {
                                                    console.log("error: ", err);
                                                    result(err, null);
                                                    return;
                                                }
                                                else {
                                                    result(null, res);
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

User.checkUnique = (email, result) => {
    sql.query(`SELECT id FROM app_user WHERE email = ?`, email, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found user: ", res[0]);
            result(null, res[0]);
            return;
        }

        result({
            kind: "not_found"
        }, null);
    })
}

User.findBy = (userId, result) => {
    sql.query(`SELECT * FROM app_user WHERE id = ?`, [ parseInt(userId) ], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found user: ", res[0]);
            result(null, res[0]);
            return;
        }

        result({
            kind: "not_found"
        }, null);

    });
};

User.getTests = (userId, result) => {
    sql.query(`SELECT a.isDownloadable AS downloadAvailable, a.guid, b.name AS virus, c.name AS test, d.name AS result, d.is_safe, a.date_recorded, a.remarks FROM history a JOIN virus_type b ON a.virusttypeid = b.id JOIN test_type c ON a.testtypeid = c.id JOIN result_type d ON a.resulttype = d.id WHERE a.appuserid = ? ORDER BY a.date_recorded DESC LIMIT 5`, userId, (err, res) => {
        if (err) {
            console.log(err);
            result(err, null);
            return;
        }
        else if (res.length) {
            console.log(res);
            res.forEach(item => {
                console.log(item.date_recorded);
                // let date = moment(item.date_recorded).add(8, 'hours').format("YYYY-MM-DDTHH:mm:ss");
                let date = moment(item.date_recorded).format("YYYY-MM-DDTHH:mm:ss");
                console.log(date);
                item.date_recorded = date;
            });
            result(null, res);
        }
        else {
            result(null, []);
        }
    })
}

User.findByUser = (appUserName, result) => {
    sql.query(`SELECT * FROM app_user where username = ?`, appUserName.toString(), (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            console.log("found user: ", res[0]);
            result(null, res[0]);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    })
}

User.getByGuid = (guid, result) => {
    sql.query(`SELECT a.id, a.firstname, a.lastname FROM app_user a JOIN app_user_guid b ON a.id = b.appuserid WHERE b.guid = ?`, guid.toString(), (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            console.log("found user: ", res[0]);
            result(null, res[0]);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    })
}

User.updateToken = (id, token, result) => {
    console.log('i update token');
    sql.query(`UPDATE app_user SET accessToken = ? WHERE id = ?`, [ token.toString(), parseInt(id) ], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res);
    });
}

User.updateDeviceId = (id, deviceId, result) => {
    console.log(deviceId);
    sql.query(`UPDATE app_user SET deviceId = ? WHERE id = ?`, [ deviceId.toString(), parseInt(id)], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        }
        result(null, res);
    });
}

User.updatePassword = (email, password, otp, result) => {
    sql.query(`SELECT * FROM app_user WHERE email = ? AND otp = ?`, [email, otp], (err, checkres) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (checkres.length) {
            const id = checkres[0].id;
            sql.query(`UPDATE app_user SET password = ?, otp = NULL WHERE id = ?`, [password.toString(), parseInt(id)], (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                result(null, res);
                return;
            })
        }
        else {
            result({
                kind: "not_found"
            }, null);
        }
    })
}

User.updateGFbId = (id, googleid, facebookid, appleid, result) => {
    if (googleid) {
        sql.query(`UPDATE app_user SET googleid = ? WHERE id = ?`,[googleid, id], (err, res) => {
            if (err) {
                console.log(err);
                result(err, null);
                return;
            }
            else {
                result(null, res);
            }
        })
    }
    else if (facebookid) {
        sql.query(`UPDATE app_user SET facebookid = ? WHERE id = ?`,[facebookid, id], (err, res) => {
            if (err) {
                console.log(err);
                result(err, null);
                return;
            }
            else {
                result(null, res);
            }
        })
    }
    else {
        sql.query(`UPDATE app_user SET appleid = ? WHERE id = ?`,[appleid, id], (err, res) => {
            if (err) {
                console.log(err);
                result(err, null);
                return;
            }
            else {
                result(null, res);
            }
        })
    }  
}

User.findRole = (userId, result) => {
    sql.query(`SELECT b.name FROM app_user_role a JOIN user_role b ON a.userroleid = b.id WHERE appuserid = ?`, parseInt(userId), (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res[0]);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    });
}

User.getGuid = (userId, result) => {
    sql.query(`SELECT guid FROM app_user_guid WHERE appuserid = ?`, parseInt(userId), (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res[0]);
    })
}

User.deleteToken = (id, result) => {
    sql.query(`UPDATE app_user SET accessToken = null WHERE id = ?`, parseInt(id), (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res);
    })
}

User.contactTrace = (userId, targetGuid, result) => {
    sql.query(`SELECT appuserid FROM app_user_guid WHERE guid = ?`, targetGuid, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else {
            let currentTime = moment.utc().format();
            sql.query(`INSERT INTO contact_tracing SET sourceid = ?, targetid = ?, method = 'Automatic', dateRecorded = ?`, [parseInt(userId), parseInt(res[0].appuserid), currentTime], (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                console.log(res);
                result(null, res);
            })
        }
    })
}


User.toggleAccepted = (userId, result) => {
    sql.query(`UPDATE app_user SET isAccepted = true, dateAccepted = NOW() WHERE id = ?`, parseInt(userId), (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res);
    })
}
module.exports = User;