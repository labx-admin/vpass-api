const sql = require("./db");
const crypto = require('crypto');
const User = require("./user");
const moment = require("moment");

const Test = function(test) {
    this.appuserid = test.appuserid;
    this.virusttypeid = test.virusttypeid;
    this.testtypeid = test.testtypeid;
    this.resulttype = test.resulttype;
    this.remarks = test.remarks;
    this.guid = test.guid;
    this.date_recorded = test.date_recorded;
    this.facilitatorid = test.facilitatorid;
    this.isDownloadable = test.isDownloadable;
}

Test.create = (id, guid, test, result) => {
    User.getByGuid(guid, (err, userGuid) => {
        if (err) {
            result(err, null);
            return;
        }
        else {
            sql.query(`SELECT guid FROM history`, (err, guidList) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                else {
                    let newGuid = crypto.randomBytes(10).toString('hex');
                    while(guidList.includes(newGuid)) {
                        newGuid = crypto.randomBytes(10).toString('hex');
                    }
                    console.log(test);
                    let currentTime = moment(test.date_recorded).utc().format();
                    console.log(currentTime);
                    const newHistory = new Test({
                        appuserid: userGuid.id,
                        virusttypeid: test.virusttypeid,
                        testtypeid: test.testtypeid,
                        resulttype: test.resulttype,
                        remarks: test.remarks,
                        guid: newGuid,
                        date_recorded: currentTime,
                        facilitatorid: id,
                        isDownloadable: test.isDownloadable
                    })
                    sql.query(`INSERT INTO history SET ?`, newHistory, (err, res) => {
                        if (err) {
                            console.log("error: ", err);
                            result(err, null);
                            return;
                        }
                        else {
                            result(null, {
                                ...test
                            })
                        }
                    })
                }
            })
        }
    })
}

Test.toggleDownloadable = (guid, result) => {
    sql.query(`UPDATE history SET isDownloadable = NOT isDownloadable WHERE guid = ?`, guid.toString(), (err, res) => {
        if (err) {
            console.log(err);
            result(err, null);
            return;
        }
        else {
            result(null, res);
        }
    })
}

Test.getData = (result) => {
    sql.query(`SELECT a.id AS virusttypeid, a.name AS virus, b.id AS testtypeid, b.name AS test, c.id AS resulttypeid, c.name AS result, c.is_safe FROM virus_type a JOIN test_type b ON b.virusttypeid = a.id JOIN result_type c ON c.testtypeid = b.id where a.is_active = 1`, (err, res) => {
        if (err) {
            console.log(err);
            result(err, null);
            return;
        }
        else {
            result(null, res);
        }
    })
}

Test.getUserData = (guid, result) => {
    sql.query(`SELECT a.firstname, a.lastname, a.city_address FROM app_user a JOIN app_user_guid b ON a.id = b.appuserid WHERE b.guid = ?`, guid.toString(), (err, res) => {
        if (err) {
            console.log(err);
            result(err, null);
            return;
        }
        else if (res.length < 1) {
            result({kind: "not_found"}, null);
        }
        else {
            result(null, res[0]);
        }
    })
}

Test.searchUser = (query, result) => {
    sql.query(`SELECT a.firstname, a.lastname, a.email, a.contact_number, b.guid FROM app_user a JOIN app_user_guid b ON a.id = b.appuserid WHERE CONCAT(a.firstname, ' ', a.lastname) LIKE '%${query}%' OR a.email LIKE '%${query}%'`, (err, res) => {
        if (err) {
            console.log(err);
            result(err, null);
            return;
        }
        else if (res.length < 1) {
            result({kind: "not_found"}, null);
        }
        else {
            result(null, res);
        }
    })
}
Test.getTestResult = (id, guid, result) => {
    console.log('id', id);
    sql.query(`SELECT a.id FROM app_user a JOIN app_user_guid b ON a.id = b.appuserid WHERE b.guid = ?`, guid.toString(), (err, guidCheck) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else if (guidCheck.length < 1) {
            result({kind: "not_found"}, null);
        }
        else {
            const scannedId = guidCheck[0].id;
            const establishmentId = parseInt(id) || null;
            let currentTime = moment.utc().format();
            sql.query(`INSERT INTO contact_tracing SET sourceid = ?, targetid = ?, method = 'Manual', dateRecorded = ?`,[ establishmentId, parseInt(scannedId), currentTime ], (err, logContact) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                else {
                    sql.query(`SELECT a.isDownloadable, f.firstname, f.lastname, f.city_address, f.contact_number, c.name AS virus, d.name AS test, e.name AS result, e.is_safe, a.date_recorded, a.remarks, a.guid FROM history a JOIN app_user_guid b ON a.appuserid = b.appuserid JOIN virus_type c ON a.virusttypeid = c.id JOIN test_type d ON a.testtypeid = d.id JOIN result_type e ON a.resulttype = e.id JOIN app_user f ON f.id =  a.appuserid WHERE b.guid = ? ORDER BY a.date_recorded DESC LIMIT 4`, guid.toString(), (err, res) => {
                        if (err) {
                            console.log("error: ", err);
                            result(err, null);
                            return;
                        }
                        if (res.length) {
                            res.forEach(item => {
                                console.log(item.date_recorded);
                                // let date = moment(item.date_recorded).add(8, 'hours').format("YYYY-MM-DDTHH:mm:ss");
                                let date = moment(item.date_recorded).format("YYYY-MM-DDTHH:mm:ss");
                                console.log(date);
                                item.date_recorded = date;
                            });
                            result(null, res);
                            return;
                        }
                        else {
                            sql.query(`SELECT a.firstname, a.lastname, a.city_address, a.contact_number FROM app_user a JOIN app_user_guid b ON a.id = b.appuserid WHERE b.guid = ?`, guid.toString(), (err, nores) => {
                                if (err) {
                                    console.log("error: ", err);
                                    result(err, null);
                                    return;
                                }
                                else {
                                    nores[0].virus = null;
                                    nores[0].test = null;
                                    nores[0].result = null;
                                    nores[0].is_safe = 2;
                                    nores[0].date_recorded = null;
                                    nores[0].remarks = null;
                                    nores[0].isDownloadable = false;
                                    result(null, nores);
                                    return;
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

Test.getAllUserResults = (guid, result) => {
    console.log('got in api');
    sql.query(`SELECT a.firstname, a.lastname, a.city_address, a.contact_number FROM app_user a JOIN app_user_guid b ON a.id = b.appuserid WHERE b.guid = ?`, guid.toString(), (err, userDetails) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else {
            sql.query(`SELECT c.name AS virus, d.name AS test, e.name AS result, e.is_safe, a.date_recorded, a.remarks, a.guid FROM history a JOIN app_user_guid b ON a.appuserid = b.appuserid JOIN virus_type c ON a.virusttypeid = c.id JOIN test_type d ON a.testtypeid = d.id JOIN result_type e ON a.resulttype = e.id WHERE b.guid = ? ORDER BY a.date_recorded DESC`, guid.toString(), (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                if (res.length) {
                    res.forEach(item => {
                        console.log(item.date_recorded);
                        // let date = moment(item.date_recorded).add(8, 'hours').format("YYYY-MM-DDTHH:mm:ss");
                        let date = moment(item.date_recorded).format("YYYY-MM-DDTHH:mm:ss");
                        console.log(date);
                        item.date_recorded = date;
                    });
                    userDetails[0].results = res;
                    result(null, userDetails[0]);
                    return;
                }
                else {
                    const noresults = {
                        virus: null,
                        test: null,
                        result: null,
                        is_safe: 2,
                        date_recorded: null,
                        remarks: null
                    }
                    userDetails[0].results = noresults;
                    result(null, nores[0]);
                    return;
                }
            })
        }
    })
}

Test.getResultsByMonth = (month, result) => {
    sql.query(`SELECT a.date_recorded, b.name AS virus, c.name AS test, d.name AS result, d.is_safe FROM history a JOIN virus_type b ON a.virusttypeid = b.id JOIN test_type c ON a.testtypeid = c.id JOIN
    result_type d ON a.resulttype = d.id WHERE MONTH(a.date_recorded) = ?`, parseInt(month), (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else {
            result(null, res);
        }
    })
}

Test.updateResult = (testGuid, virusttypeid, testtypeid, resulttype, date_recorded, isDownloadable, result) => {
    sql.query(`UPDATE history SET virusttypeid = ?, testtypeid = ?, resulttype = ?, date_recorded = ?, isDownloadable = ? WHERE guid = ?`,[virusttypeid, testtypeid, resulttype, date_recorded, isDownloadable, testGuid], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else {
            result(null, res);
        }
    })
}

Test.getLatestUsers = (result) => {
    sql.query(`SELECT a.firstname, a.lastname, a.email, a.contact_number, b.guid FROM app_user a JOIN app_user_guid b ON a.id = b.appuserid ORDER BY created_at DESC LIMIT 10`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else {
            result(null, res);
        }
    })
}

Test.getUsersPagination = (page, query, result) => {
    let queryString = "";
    const perPage = 15;
    if (query != null) {
        queryString = ` WHERE CONCAT(a.firstname, ' ', a.lastname) LIKE '%${query}%' OR a.email LIKE '%${query}%'`;
        console.log(queryString);
    }
    sql.query(`SELECT COUNT(*) as count FROM app_user a` + queryString, (err, countAll) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else {
            const count = countAll[0].count;
            let skip = (page * perPage) - perPage;
            let totalPages = Math.trunc(count / perPage);
            console.log(skip);
            console.log(totalPages);
            if (skip > count) {
                skip = Math.trunc(count / perPage) * perPage;
                page = Math.trunc(count / perPage);
                if (count % perPage > 0) {
                    page += 1;
                }
            }
            if (count % perPage > 0) {
                totalPages += 1;
            }
            console.log(skip);
            console.log(totalPages);
            sql.query(`SELECT a.firstname, a.lastname, a.email, a.contact_number, b.guid FROM app_user a JOIN app_user_guid b ON a.id = b.appuserid ` + queryString + ` ORDER BY created_at DESC LIMIT ?, ?`, [skip, perPage], (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                else {
                    const data = {
                        totalPages: totalPages,
                        currrentPage: page,
                        results: res
                    }
                    result(null, data);
                }
            })

        }
    })
}

Test.getTestByGuid = (testGuid, result) => {
    sql.query(`SELECT a.firstname, a.lastname, a.contact_number, c.name AS virus, d.name AS test, e.name AS result, e.is_safe, b.date_recorded, b.guid FROM app_user a JOIN history b ON a.id = b.appuserid JOIN virus_type c ON b.virusttypeid = c.id JOIN test_type d ON b.testtypeid = d.id JOIN result_type e ON b.resulttype = e.id WHERE b.guid = ?`, testGuid, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            res.forEach(item => {
                console.log(item.date_recorded);
                // let date = moment(item.date_recorded).add(8, 'hours').format("YYYY-MM-DDTHH:mm:ss");
                let date = moment(item.date_recorded).format("YYYY-MM-DDTHH:mm:ss");
                console.log(date);
                item.date_recorded = date;
            });
            result(null, res[0]);
            return;
        }
        else {
            const noresults = {
                firstname: null,
                lastname: null,
                contact_number: null,
                virus: null,
                test: null,
                result: null,
                is_safe: 2,
                date_recorded: null,
                remarks: null
            }
            result(null, noresults);
            return;
        }
    })
}

Test.deleteTest = (testGuid, result) => {
    sql.query(`DELETE FROM history WHERE guid = ?`, testGuid.toString(), (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else {
            result(null, res);
        }
    })
}

module.exports = Test;