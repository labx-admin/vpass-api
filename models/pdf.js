const fs = require('fs');
const sql = require("./db");
const hummus = require('hummus-recipe');
const docFolder = './document/';
const moment = require('moment');
const qrcode = require("qrcode");

exports.testPdf = (id, guid, facilitatorTriggered, result) => {
    let addlQuery = "";
    if (!facilitatorTriggered) {
        addlQuery = `AND a.id = ${id} `;
    }
    console.log(addlQuery);
    sql.query(`SELECT a.firstname, a.lastname, g.firstname AS facilitatorFirstName, g.lastname AS facilitatorLastName, h.prc_number, f.guid, b.date_recorded, c.name AS virus, d.name AS test, d.manufacturer, e.name AS result, e.is_safe FROM app_user a JOIN history b ON a.id = b.appuserid JOIN virus_type c ON b.virusttypeid = c.id JOIN test_type d ON b.testtypeid = d.id JOIN result_type e ON b.resulttype = e.id JOIN app_user_guid f ON a.id = f.appuserid JOIN app_user g ON g.id = b.facilitatorid JOIN facilitator_prc h ON h.appuserid = b.facilitatorid WHERE b.guid = '${guid.toString()}' ${addlQuery} AND b.isDownloadable = 1`, (err, res) => {
        if (err) {
            console.log(err);
            result(err, null);
            return;
        }
        else if (res.length < 1) {
            result({kind: "not_found"}, null);
        }
        else {
            const testData = res[0];
            let fileName = 'BLANK CERT.pdf';
            let testX = 350;
            let testY = 271;
            if (!testData.is_safe) {
                fileName = 'BLANK CERT POSITIVE.pdf';
                testX = 185;
                testY = 286;
            }
            // Create QRCode Image
            const generateQR = async text => {
                try {
                    await qrcode.toFile(docFolder + 'qrimage.png', text);
                }
                catch (err) {
                    console.log(err);
                }
            }
            generateQR("https://vpassph.com/testdetails?guid=" + guid).then(data => {
                // Create PDF
                const dateIssued = moment(testData.date_recorded).format('MMMM DD, YYYY');
                const pdfDoc = new hummus(docFolder + fileName, docFolder + 'Test Certificate - ' + testData.firstname + ' ' + testData.lastname + '.pdf');
                pdfDoc
                .editPage(1)
                // .createPage('letter-size')
                .text(testData.virus, 125, 60, {
                    color: '#000000',
                    fontSize: 22
                })
                .text(dateIssued, 455, 190, {
                    color: '#000000'
                })
                .text(testData.virus + " " + testData.test, 35, 185, {
                    color: '#000000',
                    fontSize: 12
                })
                // .text(testData.manufacturer, 35, 185, {
                //     color: '#000000',
                //     fontSize: 12
                // })
                // .text(testData.virus + " " + testData.test, 35, 200, {
                //     color: '#000000',
                //     fontSize: 12
                // })
                .text(testData.test, testX, testY, {
                    color: '#000000'
                })
                .text(testData.result + ' for ' + testData.virus, 310, 320, {
                    color: '#F97200',
                    fontSize: 20,
                    align: 'center',
                    bold: true
                })
                .text(testData.firstname.toUpperCase() + ' ' + testData.lastname.toUpperCase(), 310, 369, {
                    color: '#000000',
                    fontSize: 20,
                    align: 'center',
                    bold: true
                })
                .image(docFolder + 'qrimage.png', 480, 45, {
                    width: 80,
                    keepAspectRatio: true
                })
                .text(testData.facilitatorFirstName + ' ' + testData.facilitatorLastName, 40, 688, {
                    color: '#000000',
                    fontSize: 11,
                    bold: false
                })
                .text(testData.prc_number, 40, 721, {
                    color: '#000000',
                    fontSize: 11,
                    bold: false
                })
                .endPage()
                // .encrypt({
                //     userPassword: testData.lastname + moment(testData.date_recorded).format('MMYYYY'),
                //     ownerPassword: '43XdLm25j@Zh5mfV',
                //     userProtectionFlag: 4
                // })
                .endPDF();
                const outputFile = `${process.cwd()}/document/Test Certificate - ${testData.firstname} ${testData.lastname}.pdf`;
                fs.unlink(docFolder + 'qrimage.png', function(err) {
                    if (err) {
                        console.log(err);
                    }
                })
                // res.status(200).sendFile(outputFile, err => {
                //     if (err) {
                //         console.log(err);
                //         res.status(500).send({
                //             message: "Error retrieving file"
                //         });
                //     }
                //     fs.unlink(outputFile, function(err) {
                //         if (err) {
                //             console.log(err);
                //         }
                //     })
                // });
                result(null, outputFile);
            });
        }
    });
}
