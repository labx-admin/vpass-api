const sql = require("./db.js");

const AppUser = function (appUser) {
    this.username = appUser.username;
    this.email = appUser.email;
    this.password = appUser.password;
    this.accessToken = appUser.accessToken;
    this.firstname = appUser.firstname;
    this.lastname = appUser.lastname;
    this.contact_number = appUser.contactNumber;
    //this.street1 = appUser.street1;
    //this.street2 = appUser.street2;
    //this.region = appUser.region;
    //this.province = appUser.province;
    this.city = appUser.city;
    this.deviceId = appUser.deviceId || null;
    //this.barangay = appUser.barangay;
}

AppUser.create = (newAppUser, newUserAcct, result) => {
    sql.query(`INSERT INTO app_user SET ?`, newAppUser, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        console.log(res);
        const createdId = res.insertId;
        const addRole = {
            appuserid: createdId,
            userroleid: 1
        }
        sql.query(`INSERT INTO app_user_role SET ?`, addRole, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }
            console.log("created user: ", {
                id: createdId,
                ...newAppUser
            });
            const addAccount = {
                appuserid: createdId,
                is_active: true
            }
            if (newUserAcct) {
                addAccount.accountnumber = newUserAcct.accountnumber;
                addAccount.accounttypeid = newUserAcct.accounttypeid;
            }
            else {
                addAccount.accounttypeid = 1;
            }
            sql.query(`INSERT INTO user_account SET ?`, addAccount, (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                result(null, {
                    id: createdId,
                    ...newAppUser
                });
            })
        })
    });
};

AppUser.findBy = (appUserId, result) => {
    sql.query(`SELECT * FROM app_user WHERE id = ${appUserId}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found user: ", res[0]);
            result(null, res[0]);
            return;
        }

        result({
            kind: "not_found"
        }, null);

    });
};


AppUser.getUserAccountsById = (appUserId, result) => {
    sql.query(`SELECT * FROM user_account WHERE appuserid = ${appUserId}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    });
};

AppUser.findRole = (appUserId, result) => {
    sql.query(`SELECT b.name FROM app_user_role a JOIN user_role b ON a.userroleid = b.id WHERE appuserid = ${appUserId}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res[0]);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    });
}

//Findby function to search for user by username, used by auth controller
AppUser.findByUser = (appUserName, result) => {
    sql.query(`SELECT * FROM app_user where username = "${appUserName}"`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            console.log("found user: ", res[0]);
            result(null, res[0]);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    })
}

AppUser.findByUserAccountNumber = (accountnumber, result) => {
    console.log(accountnumber);
    sql.query(`SELECT a.id, a.username, a.firstname, a.lastname, b.id AS useraccountid, b.accountnumber, d.name AS accounttype, c.meter_number FROM app_user a JOIN user_account b ON a.id = b.appuserid LEFT JOIN electric_meter c ON b.electricmeterid = c.id JOIN account_type d ON d.id = b.accounttypeid WHERE b.accountnumber = '${accountnumber}'`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            console.log("found user: ", res[0]);
            result(null, res[0]);
            return;
        }
        console.log(res);
        result({
            kind: "not_found"
        }, null);
    })
}

AppUser.getAll = result => {
    sql.query(`SELECT a.id, a.username, a.email, b.accountnumber, a.firstname, a.lastname, a.contact_number, a.street1, a.street2, a.region, a.province, a.city, a.barangay, a.created_at, a.updated_at FROM app_user a JOIN user_account b ON a.id = b.appuserid`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("app users: ", res);
        result(null, res);
    });
};

AppUser.search = (searchString, result) => {
    let searchQuery = '';
    if (searchString) {
        searchQuery = `WHERE a.firstname LIKE '%${searchString}%' OR a.lastname LIKE '%${searchString}%' OR a.username LIKE '%${searchString}%' OR b.accountnumber LIKE '%${searchString}%'`
    }
    sql.query(`SELECT a.id, a.username, a.email, b.accountnumber, c.meter_number, a.firstname, a.lastname, a.contact_number, a.street1, a.street2, a.region, a.province, a.city, a.barangay, a.created_at, a.updated_at FROM app_user a JOIN user_account b ON a.id = b.appuserid LEFT JOIN electric_meter c ON b.electricmeterid = c.id ${searchQuery}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("app users: ", res);
        result(null, res);
    })
}

AppUser.getAccountTypes = result => {
    sql.query(`SELECT * FROM account_type`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res);
    })
}

AppUser.updateToken = (id, token, result) => {
    sql.query(`UPDATE app_user SET accessToken = "${token}" WHERE id = ${id}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res);
    });
}

AppUser.deleteToken = (id, result) => {
    sql.query(`UPDATE app_user SET accessToken = null WHERE id = ${id}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res);
    })
}

AppUser.updateById = (id, appUser, accountnumber, result) => {
    console.log('before update', appUser);
    sql.query(`UPDATE app_user SET username = ?, email = ?, firstname = ?, lastname = ?, contact_number = ?, 
                    street1 = ?, street2 = ?, region = ?, province = ?, city = ?, barangay = ? WHERE id = ?`, [
        appUser.username, appUser.email, appUser.firstname, appUser.lastname, appUser.contact_number, appUser.street1,
                    appUser.street2, appUser.region, appUser.province, appUser.city, appUser.barangay, id
    ], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        else if (res.affectedRows == 0) {
            result({
                kind: "not_found"
            }, null);
        }
        else if (accountnumber) {
            sql.query(`UPDATE user_account SET accountnumber = '${accountnumber}' WHERE appuserid = ${id}`, (err2, res2) =>{
                if (err2) {
                    console.log("error: ", err2);
                    result(err2, null);
                    return;
                }
        
                else if (res2.affectedRows == 0) {
                    result({
                        kind: "not_found"
                    }, null);
                }
                else {
                    console.log("updated user: ", {
                        id: id,
                        ...appUser
                    });
            
                    result(null, {
                        id: id,
                        ...appUser
                    })
                }
            })
        }
        else {
            console.log("updated user: ", {
                id: id,
                ...appUser
            });
    
            result(null, {
                id: id,
                ...appUser
            })
        }
    });
};


AppUser.remove = (id, result) => {
    console.log("id to remove: ", id);
    sql.query(`DELETE FROM app_user_role WHERE appuserid = ?`, id, (err, res) => {
        console.log(res);
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.affectedRows == 0) {
            result({
                kind: "not_found"
            }, null);
        }
        sql.query(`DELETE FROM user_account WHERE appuserid = ?`, id, (err2, res2) => {
            if (err2) {
                console.log("error: ", err2);
                result(err2, null);
                return;
            }
    
            if (res2.affectedRows == 0) {
                result({
                    kind: "not_found"
                }, null);
            }
            console.log("deleted user: ", id);

            sql.query(`DELETE FROM app_user WHERE id = ?`, id, (err3, res3) => {
                if (err3) {
                    console.log("error: ", err3);
                    result(err3, null);
                    return;
                }
        
                if (res3.affectedRows == 0) {
                    result({
                        kind: "not_found"
                    }, null);
                }
                console.log("deleted user: ", id);
    
                result(null, id);
            })
        })
    });
}

module.exports = AppUser;

// sql.query(``)
