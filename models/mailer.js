const nodemailer = require('nodemailer');
const sql = require("./db");

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'vpass-support@labx.asia',
        pass: 'lpnvmfjgycwqldji'
    }
});

exports.sendResetEmail = (email, result) => {
    sql.query(`SELECT id from app_user WHERE email = ?`, email, (err, res) => {
        if (err) {
            console.log(err);
            result(err, null);
            return;
        }
        else {
            const generatedOTP = Math.floor(100000 + Math.random() * 900000);
            sql.query(`UPDATE app_user SET otp = ? WHERE email = ?`, [generatedOTP, email], (err, res2) => {
                if (err) {
                    console.log(err);
                    result(err, null);
                    return;
                }
                else {
                    // const endpoint = 'http://localhost:4200';
                    const endpoint = 'https://vpassph.com';
                    const link = endpoint + '/resetpassword?email=' + encodeURIComponent(email) + '&otp=' + generatedOTP;
                    const mailOptions = {
                        from: 'vpass-support@labx.asia',
                        to: email,
                        subject: 'Forgot Password Link',
                        text: `Hello, You are receiving this email because we received a password reset request for your account.
                         If you did not request a password reset, no further action is required. Please use this link to reset your
                         password. ` + link + ` Regards, VPass Team`,
                        html: `<p>Hello</p><br>
                        <p>You are receiving this email because we received a password reset request for your account.
                         If you did not request a password reset, no further action is required.</p>
                        <p>Please use this <a href='` + link + `'>link</a> to reset your password.</p><br>
                        <p>Regards,</p>
                        <p>VPass Team</p>`
                    }
                    transporter.sendMail(mailOptions, function(error, info) {
                        if (error) {
                            console.log(error);
                            result(error, null);
                            return;
                        }
                        else {
                            result(null, "Email has been sent");
                            return;
                        }
                    })
                }
            })
        }
    })
}