module.exports = app => {
    const validation = require("../controllers/validation.controller");
    const test = require("../controllers/test.controller");
    const testpdf = require("../models/pdf");

    app.get("/checkpoint/result/:guid", [
        validation.verifyToken,
        test.getResults
    ])

    app.get("/checkpoint/webresult/:guid", [
        test.getHTMLResults
    ])

    app.get("/webresult/:guid", [
        test.getWebResults
    ])

    app.get("/mobileresult/:guid", [
        validation.verifyToken,
        test.getWebResults
    ])

    app.get("/testdetail/:guid", [
        test.getTestByGuid
    ]);
}