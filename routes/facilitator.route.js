module.exports = app => {
    const validation = require("../controllers/validation.controller");
    const test = require("../controllers/test.controller");
    const auth = require("../controllers/auth.controller");
    const user = require("../controllers/user.controller");

    app.post("/facilitator/login", [
        auth.validateAuthFields,
        auth.verifyPassword,
        auth.loginAdmin
    ]);

    app.post("/facilitator/addtest", [
        validation.verifyToken,
        validation.verifyFacilitator,
        test.addResult
    ])

    app.get("/facilitator/user/:guid", [
        validation.verifyToken,
        validation.verifyFacilitator,
        test.getUserData
    ])

    app.get("/facilitator/user/search/:query", [
        validation.verifyToken,
        validation.verifyFacilitator,
        test.searchUser
    ])

    app.get("/facilitator/data", [
        validation.verifyToken,
        validation.verifyFacilitator,
        test.getData
    ])

    app.get("/facilitator/report/:month", [
        validation.verifyToken,
        validation.verifyFacilitator,
        test.getReport
    ])

    app.get("/facilitator/getuserresults/:guid", [
        validation.verifyToken,
        validation.verifyFacilitator,
        test.getAllUserResults
    ])

    app.put("/facilitator/toggledownload", [
        validation.verifyToken,
        validation.verifyFacilitator,
        test.toggleDownloadable
    ]);

    app.put("/facilitator/updateresult", [
        validation.verifyToken,
        validation.verifyFacilitator,
        test.updateResult
    ])

    app.get("/facilitator/latestusers", [
        validation.verifyToken,
        validation.verifyFacilitator,
        test.getLatestUsers
    ])

    app.get("/facilitator/downloadpdf/:guid", [
        validation.verifyToken,
        validation.verifyFacilitator,
        user.downloadOtherPDF
    ])

    app.delete("/facilitator/deletetest/:guid", [
        validation.verifyToken,
        validation.verifyFacilitator,
        test.deleteTest
    ])

    app.post("/facilitator/users/search", [
        validation.verifyToken,
        validation.verifyFacilitator,
        test.getUsersPagination
    ])
}