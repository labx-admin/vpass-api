module.exports = app => {
    const validation = require("../controllers/validation.controller");
    const auth = require("../controllers/auth.controller");
    const user = require("../controllers/user.controller");
    
    app.post("/user/register", [
        user.register
    ]);

    app.post("/user/thirdpartysignin", [
        user.gfbSignin
    ])

    app.post("/checkpoint/register", [
        user.registerCheckpoint
    ]);
    
    app.post("/facilitator/register", [
        user.registerFacilitator
    ])

    app.post("/user/login", [
        auth.validateAuthFields,
        auth.verifyPassword,
        auth.login
    ]);

    app.get("/user/guid", [
        validation.verifyToken,
        user.getGuid
    ]);

    app.get("/user/results", [
        validation.verifyToken,
        user.getTestResults
    ])

    app.post("/user/logout", [
        validation.verifyToken,
        auth.logout
    ]);

    app.get("/user/resetrequest/:email", [
        user.resetEmail
    ])

    app.post("/user/resetpassword", [
        user.resetPassword
    ])

    app.post("/user/contacttrace", [
        validation.verifyToken,
        user.contactTracing
    ])

    app.get("/user/download/:guid", [
        validation.verifyToken,
        user.downloadPDF
    ]);

    app.get("/user/mobiledownload/:guid", [
        validation.verifyToken,
        user.mobileDownload
    ])

    app.delete("/user/delete/:id", [
        validation.verifyToken,
        validation.verifyFacilitator,
        user.delete
    ]);

    app.put("/user/edit", [
        validation.verifyToken,
        validation.verifyFacilitator,
        user.update
    ]);
}