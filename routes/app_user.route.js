module.exports = app => {
    const appUser = require("../controllers/app_user.controller");
    const auth = require("../controllers/auth.controller");
    const validation = require("../controllers/validation.controller");

    app.post("/appUser", appUser.create);

    app.post("/appUser/create", [
        validation.verifyToken,
        validation.verifyAdmin,
        appUser.adminCreate
    ]);
    //Validates auth fields exist in body, verifies password then creates jwt token
    app.post("/appUser/login", [
        auth.validateAuthFields,
        auth.verifyPassword,
        auth.login
    ]);

    //Verifies auth token then proceeds with update
    app.put("/appUser", [
        validation.verifyToken,
        appUser.update
    ]);

    app.put("/admin/appUser/update", [
        validation.verifyToken,
        validation.verifyAdmin,
        appUser.update
    ]);

    app.get("/appUser/all", [
        validation.verifyToken,
        validation.verifyAboveUser,
        appUser.findAll
    ]);

    app.get("/appUser/accounts", [
        validation.verifyToken,
        appUser.getUserAccounts
    ]);

    app.get("/appUser", [
        validation.verifyToken,
        appUser.findOne
    ]);

    app.delete("/appUser", [
        validation.verifyToken,
        appUser.delete
    ]);

    app.post("/appUser/logout", [
        validation.verifyToken,
        auth.logout
    ]);

    app.delete("/appUser/:id", [
        validation.verifyToken,
        validation.verifyAboveUser,
        appUser.deleteOtherUser
    ]);

    app.post("/admin/appUser/pagesearch", [
        validation.verifyToken,
        validation.verifyAdmin,
        appUser.paginationSearch
    ]);

    app.get("/admin/appUser/getUserAccountDetails/:accountnumber", [
        validation.verifyToken,
        validation.verifyAdmin,
        appUser.getAccountDetails
    ]);

    // app.get("/accountTypes", [
    //     validation.verifyToken,
    //     appUser.getAccountTypes
    // ]);
}